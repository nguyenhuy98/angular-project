package com.example.bevss.repository;

import com.example.bevss.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity , Long> {

    List<ProductEntity> findAllByType(@Param("type") String type);

    @Query(" select e " +
            " from ProductEntity e " +
            " where (:type is null or e.type = :type) " +
            "   and (:keyword is null or e.name like %:keyword%) ")
    List<ProductEntity> findByType(@Param("type") String type, @Param("keyword") String keyword);

    ProductEntity findByCode(String code);

    List<ProductEntity> findByIdIn(List<Long> ids);
}
