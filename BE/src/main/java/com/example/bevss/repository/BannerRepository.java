package com.example.bevss.repository;

import com.example.bevss.entity.BannerEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BannerRepository extends JpaRepository<BannerEntity, Long> {

    Page<BannerEntity> findByStatus(Integer status, Pageable pageable);
}
