package com.example.bevss.repository;

import com.example.bevss.dto.CommentDTO;
import com.example.bevss.entity.CommentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<CommentEntity,Long> {



    @Query(value = "select co.id as CommentId , co.content as content , " +
            " co.product_id as  productId , u.username as username , co.user_id as userId from comment co " +
            " left join user u on u.id = co.user_id where " +
            "  co.product_id = :id order by created_at desc" , nativeQuery = true)
    List<CommentDTO> findByProduct(Long id);
}
