package com.example.bevss.repository;

import com.example.bevss.dto.OrderDTO;
import com.example.bevss.dto.ChartDTO;
import com.example.bevss.dto.SearchOrderDTO;
import com.example.bevss.entity.OrderEntity;
import com.example.bevss.entity.ProductEntity;
import com.example.bevss.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<OrderEntity, Long> {

    @Query(value = "select o.id as orderId, us.username as name , us.phone , o.created_time" +
            " from user us join orders o  on us.id = o.user_id " +
            " where :phone is null or  us.phone like  %:phone% ",nativeQuery = true)
    List<SearchOrderDTO> listOrder(@Param("phone") String phone);

    @Query(value = "SELECT p.name as name  , p.price as price, oo.quantity as quantity,p.image as image FROM order_product oo  JOIN product p ON p.id = oo.product_id " +
            " WHERE oo.order_id = :id ",nativeQuery = true)
    List<OrderDTO>  orderDetail(Long id);





    @Query(" select new com.example.bevss.dto.ChartDTO(monthname(o.createdTime), sum(r.quantity * p.price) ) " +
            " from OrderEntity o " +
            "   inner join OrderProductRelationshipEntity r on o.id = r.order.id " +
            "   inner join ProductEntity p on r.product.id = p.id " +
            " where " +
            "   year(o.createdTime) = :year " +
            " group by month(o.createdTime) " +
            " order by o.createdTime ")
    List<ChartDTO> getMonthlyReport(@Param("year") Integer year);

    @Query( "select u from OrderEntity u  where u.user.id = :userId" )
    List<OrderEntity> findAllByUser(@Param("userId") Long userId);

    @Query( "select e " +
            " from OrderEntity e " +
            " where :keyword is null " +
            "   or concat(e.id,'') like %:keyword% " +
            "   or e.phone like %:keyword% " +
            "   or concat(e.lastName, ' ', e.firstName) like %:keyword% " +
            " order by e.createdTime desc " )
    List<OrderEntity> findByKeyword(@Param("keyword") String keyword);
}
