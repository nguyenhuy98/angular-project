package com.example.bevss.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Table(name = "banner")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class BannerEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @NotBlank
    @Column(name = "name")
    private String name;

    @Column(name = "slug")
    private String slug;

    @Column(name = "title")
    private String title;

    @Column(name = "link")
    private String link;

    @Column(name = "status")
    private Integer status;

    @Column(name = "descript")
    private String descript;

    @NonNull
    @NotBlank
    @Column(name = "image")
    private String image;

}
