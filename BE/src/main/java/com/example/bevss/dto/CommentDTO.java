package com.example.bevss.dto;

public interface CommentDTO {
    Long getCommentId();
    String getContent();

    Long getUserId();

    Long getProductId();

    String getUsername();
}
