package com.example.bevss.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
public class RegisterRequest {
    @NotBlank
    @Size(min = 6, max = 20)
    private String password;


    @NotBlank(message = "Tên đăng nhập không được để trống")
    @Size(min = 2, max = 50, message = "Tên đăng nhập phải từ 2 đến 50 ký tự")
    @Pattern(regexp = "^[a-zA-Z0-9]+$", message = "Tên đăng nhập chỉ gồm các ký tự các chữ cái và số và không đươc bỏ trống")
    private String username;


    private String fullName;
    @Email(message = "Mời bạn nhập đúng định dạng email")
    @NotBlank(message = "Email is không được để trống")
    @Length(max = 50, message = "Nhập tối đa 50 ký tự")
    private String email;


    private String phone;
    private String address;
}
