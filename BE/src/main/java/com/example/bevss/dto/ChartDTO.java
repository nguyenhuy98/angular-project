package com.example.bevss.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChartDTO {

    private String label;

    private BigDecimal y;

    public ChartDTO(Integer label, BigDecimal y) {
        this.label = String.valueOf(label);
        this.y = y;
    }
}
