package com.example.bevss.dto;


import java.time.LocalDateTime;

public interface SearchOrderDTO {

    Long getOrderId();
    String getName();
    String getPhone();
    LocalDateTime getCreatedTime();

}
