package com.example.bevss.controller;

import com.example.bevss.dto.OrderDTO;
import com.example.bevss.dto.OrderRequest;
import com.example.bevss.dto.SearchOrderDTO;
import com.example.bevss.entity.OrderEntity;
import com.example.bevss.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/order")
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;

    @GetMapping()
    ResponseEntity<List<OrderEntity>> listOrder(@RequestParam(required = false) String keyword){
        return ResponseEntity.ok(orderService.getAll(keyword));
    }

    @GetMapping("/{id}")
    ResponseEntity<List<OrderDTO>> orderDetail(@PathVariable Long id ){
        return ResponseEntity.ok(orderService.orderDetail(id));
    }

    @PostMapping("")
    OrderEntity orderCreate(@RequestBody @Valid OrderRequest request){
        return orderService.createOrder(request);
    }

    @GetMapping("/user")
    ResponseEntity<List<OrderEntity>> getOrderByUserId(@RequestParam("userId") Long userId){
        return ResponseEntity.ok(orderService.getOrderByUser(userId));
    }

    @DeleteMapping("/{id}")
    ResponseEntity<OrderEntity> delete(@PathVariable Long id ){
        return ResponseEntity.ok(orderService.delete(id));
    }


    @GetMapping("/detail/{id}")
    ResponseEntity<OrderEntity> getOrder(@PathVariable Long id) {
        return ResponseEntity.ok(orderService.detail(id));
    }


    @PutMapping
    OrderEntity orderCreate(@RequestBody @Valid OrderEntity entity){
        return orderService.updateOrder(entity);
    }
}
