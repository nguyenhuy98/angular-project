package com.example.bevss.controller;

import com.example.bevss.entity.VoucherEntity;
import com.example.bevss.service.VoucherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/voucher")
public class VoucherController {

    @Autowired
    private VoucherService voucherService;

    @GetMapping("/{id}")
    ResponseEntity<VoucherEntity> getVoucherById(@PathVariable Long id){
        return ResponseEntity.ok(voucherService.getDetail(id));
    }


    @PostMapping("/")
    VoucherEntity createVoucher(@RequestBody VoucherEntity voucherEntity){
        return voucherService.createVoucher(voucherEntity);
    }

    @PutMapping("/")
    VoucherEntity updateVoucher(@RequestBody VoucherEntity voucherEntity){
        return  voucherService.updateVoucher(voucherEntity);
    }

    @GetMapping("/list")
    ResponseEntity<List<VoucherEntity>> getAllVoucher(){
        return ResponseEntity.ok(voucherService.getAll());
    }

    @DeleteMapping("/")
    Boolean deleteVoucher(@PathVariable Long id){
        return voucherService.deleteVoucher(id);
    }
}
