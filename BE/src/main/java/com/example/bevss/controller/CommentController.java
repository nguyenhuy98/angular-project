package com.example.bevss.controller;


import com.example.bevss.dto.CommentDTO;
import com.example.bevss.entity.CommentEntity;
import com.example.bevss.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("comment")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @GetMapping("/{id}")
    ResponseEntity<CommentEntity> getCommentById(@PathVariable Long id){
        return ResponseEntity.ok(commentService.getDetail(id));
    }


    @PostMapping("/")
    CommentEntity createComment(@RequestBody CommentEntity commentEntity){
        commentEntity.setCreatedAt(LocalDateTime.now());
        return commentService.createComment(commentEntity);
    }

    @PutMapping("/")
    CommentEntity updateComment(@RequestBody CommentEntity commentEntity){
        return  commentService.updateComment(commentEntity);
    }

    @GetMapping("/list")
    ResponseEntity<List<CommentEntity>> getAllComment(){
        return ResponseEntity.ok(commentService.getAll());
    }

    @DeleteMapping("/")
    Boolean deleteComment(@PathVariable Long id){
        return commentService.deleteComment(id);
    }


    @GetMapping("/list/{id}")
    ResponseEntity<List<CommentDTO>> getAllByProduct(@PathVariable Long id){
        return ResponseEntity.ok(commentService.getCommentByProduct(id));
    }

}
