package com.example.bevss.controller;

import com.example.bevss.entity.ProductEntity;
import com.example.bevss.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService ;

    @GetMapping()
    ResponseEntity<List<ProductEntity>> getAll(@RequestParam(required = false) String type,
                                               @RequestParam(required = false) String keyword){
        return ResponseEntity.ok(productService.getAll(type, keyword));
    }

    @GetMapping("/{id}")
    ResponseEntity<ProductEntity> getDetail(@PathVariable Long id){
        return ResponseEntity.ok(productService.getDetail(id));
    }

    @PostMapping("/")
    ProductEntity createProduct(@RequestBody ProductEntity productEntity){
        return productService.createProduct(productEntity);
    }

    @PutMapping ("/")
    ProductEntity updateProduct(@RequestBody ProductEntity productEntity){
        return productService.updateProduct(productEntity);
    }

    @DeleteMapping("/{id}")
    Boolean deleteProduct(@RequestParam Long id){
        return productService.deleteProduct(id);
    }

    @GetMapping("/category/{type}")
    ResponseEntity<List<ProductEntity>> getProductByCategory(@PathVariable String type){
        return ResponseEntity.ok(productService.getProductByCategory(type));
    }


}
