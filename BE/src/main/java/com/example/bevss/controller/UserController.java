package com.example.bevss.controller;

import com.example.bevss.dto.RegisterRequest;
import com.example.bevss.dto.ResponseModel;
import com.example.bevss.entity.UserEntity;
import com.example.bevss.service.AuthService;
import com.example.bevss.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private AuthService authService;

    @GetMapping
    public List<UserEntity> getAll() {
        return userService.getAll();
    }

    @GetMapping("/{id}")
    public UserEntity getDetail(@PathVariable Long id) {
        return userService.getDetail(id);
    }

    @PostMapping()
    public UserEntity save(@RequestBody UserEntity user) {
        return userService.save(user);
    }

    @PutMapping("/{id}")
    public UserEntity update(@RequestBody UserEntity user) {
        return userService.update(user);
    }

    @DeleteMapping("/{id}")
    public UserEntity delete(@PathVariable Long id) {
        return userService.delete(id);
    }


    @PostMapping("/signup")
    public ResponseEntity<?> registerTeacher(@Valid @RequestBody RegisterRequest request) {
        long start = System.currentTimeMillis();
        ResponseModel model = authService.register(request);
        return new ResponseEntity(model.getData(), model.getResponseStatus());
    }
}
