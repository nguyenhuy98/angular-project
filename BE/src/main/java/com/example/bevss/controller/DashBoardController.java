package com.example.bevss.controller;

import com.example.bevss.dto.ChartDTO;
import com.example.bevss.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/dash-board")
public class DashBoardController {

    @Autowired
    private OrderService orderService;

    @GetMapping()
    public List<ChartDTO> searchOrder() {
        return orderService.getMonthlyReport();
    }
}
