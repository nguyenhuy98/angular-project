package com.example.bevss.controller;

import com.example.bevss.entity.CartEntity;
import com.example.bevss.entity.CartProductRelationshipEntity;
import com.example.bevss.entity.ProductEntity;
import com.example.bevss.repository.CartRepository;
import com.example.bevss.service.CartService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cart")
@RequiredArgsConstructor


public class CartController {

    private final CartService cartService;
    private final CartRepository cartRepository;

    @GetMapping("/row")
    int getRowCart() {
        return cartService.getRowCart();
    }

    @PostMapping("/")
    CartEntity createCart(CartEntity cartEntity) {
        return cartService.createCart(cartEntity);
    }


    @PostMapping("/products/{cartId}")
    public ResponseEntity<CartProductRelationshipEntity> addProductToCart(@PathVariable("cartId") Long cartId,
                                                                          @RequestBody ProductEntity product) {
        CartProductRelationshipEntity createdRelationship = cartService.addProductToCart(cartId, product);
        return ResponseEntity.ok(createdRelationship);
    }
}
