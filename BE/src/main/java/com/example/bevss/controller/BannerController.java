package com.example.bevss.controller;

import com.example.bevss.dto.ChartDTO;
import com.example.bevss.entity.BannerEntity;
import com.example.bevss.service.BannerService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/banner")
@RequiredArgsConstructor
public class BannerController {

    @Autowired
    private BannerService bannerService;

    @GetMapping()
    public Page<BannerEntity> getBanner(Pageable pageable) {
        return bannerService.getBanner(pageable);
    }
}
