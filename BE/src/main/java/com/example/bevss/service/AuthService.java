package com.example.bevss.service;

import com.example.bevss.dto.RegisterRequest;
import com.example.bevss.dto.ResponseModel;

public interface AuthService {
    ResponseModel register(RegisterRequest request);
}
