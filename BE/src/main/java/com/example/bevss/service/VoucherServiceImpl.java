package com.example.bevss.service;

import com.example.bevss.entity.VoucherEntity;
import com.example.bevss.repository.VoucherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VoucherServiceImpl implements VoucherService{

    @Autowired
    private VoucherRepository voucherRepository;

    @Override
    public VoucherEntity createVoucher(VoucherEntity voucherEntity) {
        if(voucherEntity != null){
            return voucherRepository.save(voucherEntity);

        }
        return null;
    }

    @Override
    public VoucherEntity getDetail(Long id) {
        VoucherEntity voucherEntity = voucherRepository.findById(id).get();
        return voucherEntity == null ? null : voucherEntity ;
    }

    @Override
    public List<VoucherEntity> getAll() {
        return voucherRepository.findAll();
    }

    @Override
    public VoucherEntity updateVoucher(VoucherEntity voucherEntity) {
        if(voucherEntity != null){
            VoucherEntity  voucherEntityUpdate = voucherRepository.getById(voucherEntity.getId());
            if(voucherEntityUpdate != null){
                voucherEntityUpdate.setName(voucherEntity.getName());
                voucherEntityUpdate.setDescription(voucherEntity.getDescription());
                voucherEntityUpdate.setCode(voucherEntity.getCode());
                voucherEntityUpdate.setRate(voucherEntity.getRate());
                voucherEntityUpdate.setExemptions(voucherEntity.getExemptions());
                return voucherRepository.save(voucherEntityUpdate);
            }
        }
        return null;
    }

    @Override
    public Boolean deleteVoucher(Long id) {
        VoucherEntity voucherEntity = voucherRepository.getById(id);
        if(voucherEntity != null){
            voucherRepository.delete(voucherEntity);
            return true;
        }
        return false;
    }
}
