package com.example.bevss.service;

import com.example.bevss.dto.CommentDTO;
import com.example.bevss.entity.CommentEntity;
import com.example.bevss.repository.CommentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CommentServiceImpl implements CommentService{
    private final CommentRepository commentRepository;





    @Override
    public CommentEntity createComment(CommentEntity commentEntity) {
        if(commentEntity != null){
            return commentRepository.save(commentEntity);

        }
        return null;
    }

    @Override
    public CommentEntity getDetail(Long id) {
        CommentEntity commentEntity = commentRepository.findById(id).get();
        return commentEntity == null ? null : commentEntity ;
    }

    @Override
    public List<CommentEntity> getAll() {
        return commentRepository.findAll();
    }

    @Override
    public CommentEntity updateComment(CommentEntity commentEntity) {
        if(commentEntity != null){
            CommentEntity  commentEntityUpdate = commentRepository.getById(commentEntity.getId());
            if(commentEntityUpdate != null){
                commentEntityUpdate.setContent(commentEntity.getContent());
            }
        }
        return null;
    }

    @Override
    public Boolean deleteComment(Long id) {
        CommentEntity commentEntity = commentRepository.getById(id);
        if(commentEntity != null){
            commentRepository.delete(commentEntity);
            return true;
        }
        return false;
    }

    @Override
    public List<CommentDTO> getCommentByProduct(Long id) {
        return commentRepository.findByProduct(id);
    }
}
