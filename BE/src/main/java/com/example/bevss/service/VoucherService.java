package com.example.bevss.service;

import com.example.bevss.entity.VoucherEntity;

import java.util.List;

public interface VoucherService {

    VoucherEntity createVoucher(VoucherEntity voucherEntity);

    VoucherEntity getDetail(Long id);

    List<VoucherEntity> getAll();

    VoucherEntity updateVoucher(VoucherEntity voucherEntity);

    Boolean deleteVoucher(Long id);
}
