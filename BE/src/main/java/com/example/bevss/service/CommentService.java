package com.example.bevss.service;


import com.example.bevss.dto.CommentDTO;
import com.example.bevss.entity.CommentEntity;

import java.util.List;

public interface CommentService {

    CommentEntity createComment(CommentEntity commentEntity);

    CommentEntity getDetail(Long id);

    List<CommentEntity> getAll();

    CommentEntity updateComment(CommentEntity voucherEntity);

    Boolean deleteComment(Long id);

    // search theo tung san pham
    List<CommentDTO> getCommentByProduct(Long id);
}
