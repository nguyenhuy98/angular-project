package com.example.bevss.service;


import com.example.bevss.entity.ProductEntity;

import java.util.List;

public interface ProductService {

    ProductEntity getDetail(Long id);

    List<ProductEntity> getAll(String type, String keyword);

    ProductEntity createProduct(ProductEntity productEntity);

    ProductEntity updateProduct(ProductEntity productEntity);

    Boolean deleteProduct(Long id);

    List<ProductEntity> getProductByCategory(String category);
}
