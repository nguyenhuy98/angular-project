package com.example.bevss.service;

import com.example.bevss.dto.RegisterRequest;
import com.example.bevss.dto.ResponseModel;
import com.example.bevss.entity.UserEntity;
import com.example.bevss.enums.Role;
import com.example.bevss.exception.CommonException;
import com.example.bevss.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    @Autowired
    private UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public ResponseModel register(RegisterRequest request) {
        try {
            ResponseModel model = new ResponseModel();
            String message = "";
            boolean existUserName = userRepository.existsByUsername(request.getUsername());
            if (existUserName) {
                message = "Tên Đăng nhập đã tồn tại vui lòng đặt tên khác ";
                CommonException error = new CommonException(HttpStatus.NOT_FOUND, message);
                model.setData(error);
                model.setDescription(message);
                model.setResponseStatus(HttpStatus.BAD_REQUEST);
                return model;
            }
            // check account exist
            UserEntity newAccount = new UserEntity();
            newAccount.setPassword(passwordEncoder.encode(request.getPassword()));
            newAccount.setUsername(request.getUsername());
            newAccount.setFullName(request.getFullName());
            newAccount.setPhone(request.getPhone());
            newAccount.setAddress(request.getAddress());
            newAccount.setEmail(request.getEmail());
            newAccount.setRole(Role.GUEST);
            userRepository.save(newAccount);
            message = "Tạo tài khoản thành công!";
            CommonException success = new CommonException(HttpStatus.OK, message);
            model.setData(success);
            model.setDescription(message);
            model.setResponseStatus(HttpStatus.OK);
            return model;
        } catch (Exception e) {
            CommonException error = new CommonException(HttpStatus.BAD_REQUEST, "Lỗi Server!");
            ResponseModel model = new ResponseModel();
            model.setData(error);
            model.setDescription("Lỗi Server!");
            model.setResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            return model;
        }
    }
}