package com.example.bevss.service;

import com.example.bevss.dto.OrderDTO;
import com.example.bevss.dto.OrderRequest;
import com.example.bevss.dto.SearchOrderDTO;
import com.example.bevss.dto.ChartDTO;
import com.example.bevss.entity.OrderEntity;

import java.util.List;

public interface OrderService {

    List<OrderDTO> orderDetail(Long id);

    OrderEntity createOrder(OrderRequest request);

    OrderEntity updateOrder(OrderEntity orderEntity);

    Boolean deleteOrder(Long id);

    List<ChartDTO> getMonthlyReport();

    List<OrderEntity> getOrderByUser(Long userId);

    List<OrderEntity> getAll(String keyword);

    OrderEntity delete(Long id);

    OrderEntity detail(Long id);
}
