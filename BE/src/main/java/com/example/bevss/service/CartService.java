package com.example.bevss.service;

import com.example.bevss.entity.CartEntity;
import com.example.bevss.entity.CartProductRelationshipEntity;
import com.example.bevss.entity.ProductEntity;

public interface CartService {
    int getRowCart();
    CartEntity createCart(CartEntity cartEntity);

    CartProductRelationshipEntity addProductToCart(Long cartId, ProductEntity product);
}
