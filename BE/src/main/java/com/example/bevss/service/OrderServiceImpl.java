package com.example.bevss.service;


import com.example.bevss.constaint.MessageCodeConstaint;
import com.example.bevss.dto.*;
import com.example.bevss.entity.OrderEntity;
import com.example.bevss.entity.OrderProductRelationshipEntity;
import com.example.bevss.entity.ProductEntity;
import com.example.bevss.entity.UserEntity;
import com.example.bevss.exception.CommonException;
import com.example.bevss.repository.OrderProductRelationshipRepository;
import com.example.bevss.repository.OrderRepository;
import com.example.bevss.repository.ProductRepository;
import com.example.bevss.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;


import java.time.Year;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderProductRelationshipRepository orderProductRelationshipRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<OrderDTO> orderDetail(Long id) {
        return orderRepository.orderDetail(id);
    }

    @Override
    @Transactional
    public OrderEntity createOrder(OrderRequest request) {
        OrderEntity orderEntity = new OrderEntity();
        BeanUtils.copyProperties(request, orderEntity);
        if (request.getUserId() != null) {
            UserEntity user = userRepository.findById(request.getUserId()).orElse(null);
            orderEntity.setUser(user);
        }
        BigDecimal total = BigDecimal.ONE;
        List<Long> productIds = request.getProductOrders().stream().map(ProductOrderRequest::getProductId).collect(Collectors.toList());
        Map<Long, Integer> productOrderMap = request.getProductOrders().stream()
                .collect(Collectors.toMap(ProductOrderRequest::getProductId, ProductOrderRequest::getQuantity));
        List<ProductEntity> productEntities = productRepository.findByIdIn(productIds);
        List<OrderProductRelationshipEntity> orderProductRelationshipEntities = new ArrayList<>();
        for (ProductEntity item : productEntities) {
            Integer quantity = productOrderMap.get(item.getId());
            if (item.getQuantity() < quantity) {
                throw new CommonException(MessageCodeConstaint.SE002);
            }
            item.setQuantity(item.getQuantity() - quantity);
            total = total.add(item.getPrice().multiply(BigDecimal.valueOf(quantity)));
            OrderProductRelationshipEntity orderProductRelationshipEntity = new OrderProductRelationshipEntity();
            orderProductRelationshipEntity.setQuantity(quantity);
            orderProductRelationshipEntity.setOrder(orderEntity);
            orderProductRelationshipEntity.setPrice(item.getPrice());
            orderProductRelationshipEntity.setProduct(item);
            orderProductRelationshipEntities.add(orderProductRelationshipEntity);
        }
        orderEntity.setTotal(total);
        orderEntity.setOrderProductRelationshipEntities(orderProductRelationshipEntities);
        orderEntity.setIsPay(true);
        orderEntity.setCreatedTime(LocalDateTime.now());
        productRepository.saveAll(productEntities);
        orderEntity = orderRepository.save(orderEntity);
        orderProductRelationshipRepository.saveAll(orderProductRelationshipEntities);
        return orderEntity;
    }


    @Override
    @Transactional
    public OrderEntity updateOrder(OrderEntity orderEntity) {
        OrderEntity entity = orderRepository.getById(orderEntity.getId());
        Collection<OrderProductRelationshipEntity> list = entity.getOrderProductRelationshipEntities();
        BigDecimal total = BigDecimal.ONE;
        if (orderEntity.getOrderProductRelationshipEntities() != null) {
            for (OrderProductRelationshipEntity item : orderEntity.getOrderProductRelationshipEntities()) {
                ProductEntity productEntity;
                if (item.getId() != null) {
                    OrderProductRelationshipEntity dbOrderProduct = list.stream().filter(i -> i.getId().equals(item.getId())).findFirst().orElse(item);
                    Integer subQuantity = item.getQuantity() - dbOrderProduct.getQuantity();
                    productEntity = dbOrderProduct.getProduct();
                    productEntity.setQuantity(productEntity.getQuantity() - subQuantity);
                    list.removeIf(i -> i.getId().equals(item.getId()));
                } else {
                    productEntity = item.getProduct();
                    productEntity.setQuantity(productEntity.getQuantity() - item.getQuantity());
                    item.setOrder(entity);
                }
                item.setOrder(orderEntity);
                total = total.add(item.getPrice().multiply(BigDecimal.valueOf(item.getQuantity())));
                productRepository.save(productEntity);
            }
            orderProductRelationshipRepository.deleteAll(list);
            orderProductRelationshipRepository.saveAll(orderEntity.getOrderProductRelationshipEntities());
            orderEntity.setTotal(total);
        }
        orderRepository.save(orderEntity);
        return orderEntity;
    }

    @Override
    public Boolean deleteOrder(Long id) {
        OrderEntity orderEntity = orderRepository.getById(id);
        if (orderEntity != null) {
            orderRepository.delete(orderEntity);
            return true;
        }
        return false;
    }

    @Override
    public List<ChartDTO> getMonthlyReport() {
        Pageable pageable = PageRequest.of(0, 12);
        Integer year = Year.now().getValue();
        return orderRepository.getMonthlyReport(year);
    }

    @Override
    public List<OrderEntity> getOrderByUser(Long userId) {
        List<OrderEntity> entity;
        try {
            entity = orderRepository.findAllByUser(userId);
            return entity;
        } catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    public List<OrderEntity> getAll(String keyword) {
        List<OrderEntity> entity;
        try {
            entity = orderRepository.findByKeyword(keyword);
            return entity;
        } catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    @Transactional
    public OrderEntity delete(Long id) {
        OrderEntity orderEntity = orderRepository.getById(id);
//        if (orderEntity.getIsPay()) {
//            throw new CommonException(MessageCodeConstaint.SE003);
//        }
        orderProductRelationshipRepository.deleteAll(orderEntity.getOrderProductRelationshipEntities());
        orderRepository.delete(orderEntity);
        return new OrderEntity();
    }

    @Override
    public OrderEntity detail(Long id) {
        return orderRepository.findById(id).orElse(null);
    }
}
