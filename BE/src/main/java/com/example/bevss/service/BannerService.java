package com.example.bevss.service;

import com.example.bevss.entity.BannerEntity;
import com.example.bevss.repository.BannerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BannerService {

    @Autowired
    private BannerRepository bannerRepository;

    public Page<BannerEntity> getBanner(Pageable pageable) {
        return bannerRepository.findByStatus(1, pageable);
    }
}
