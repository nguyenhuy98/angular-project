import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product.model';
import { DashboardService } from 'src/app/service/dashboard.service';
import { Sale, TopSelling } from './saler';

@Component({
  selector: 'app-saler-ratio',
  templateUrl: './saler-ratio.component.html',
  styleUrls: ['./saler-ratio.component.scss']
})
export class SalerRatioComponent implements OnInit {
  topSelling: Sale[];
  constructor(private http: DashboardService) {

    this.topSelling = TopSelling;
    console.log(this.topSelling);

  }
  dataPoints: any = [];
  chartOptions = {
    title: {
      text: "Monthly Sales Data"
    },
    theme: "light2",
    animationEnabled: true,
    exportEnabled: true,
    axisY: {
      includeZero: true,
      valueFormatString: "$#,##0"
    },
    data: [{
      type: "column", //change type to bar, line, area, pie, etc
      yValueFormatString: "$#,##0",
      color: "#01b8aa",
      dataPoints: this.dataPoints
    }]
  }

  ngOnInit(): void {
    this.http.init().subscribe(
      res => {
        this.chartOptions = {
          title: {
            text: "Monthly Sales Data"
          },
          theme: "light2",
          animationEnabled: true,
          exportEnabled: true,
          axisY: {
            includeZero: true,
            valueFormatString: "$#,##0"
          },
          data: [{
            type: "column", //change type to bar, line, area, pie, etc
            yValueFormatString: "$#,##0",
            color: "#01b8aa",
            dataPoints: res
          }]
        }
      }
    );
  }
}
