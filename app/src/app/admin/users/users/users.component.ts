import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  displayedColumns: string[] = ['id', 'username', 'fullName', 'email', 'address', 'phone', 'actions'];
  dataSource: MatTableDataSource<User>;
  //public user: User[] ;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  Errors = { status: false, msg: '' }
  constructor(
    private userService: UserService,
    public dialog: MatDialog,
    public router: Router,
    private changeDetector: ChangeDetectorRef,
  ) { }


  ngOnInit(): void {
    const objs: any = this.getAll();
    if (objs) {
      console.log(objs);

      this.dataSource = new MatTableDataSource(objs);

      this.dataSource.paginator = this.paginator;
      console.log(this.dataSource.paginator);

      this.dataSource.sort = this.sort;
      this.changeDetector.detectChanges();
    }
  }

  getAll() {
    return this.userService.getUsers().subscribe(data => {
      this.dataSource.data = data,
      this.dataSource.paginator = this.paginator;
      console.log(this.dataSource.paginator);

      //this.dataSource.sort = this.sort;
    })
  }

  delete(id: string) {
    console.log('delete');
    console.log('id--', id);
    return this.userService.deleteUser(id).subscribe(data => {
      this.getAll()
    })
  }

  onToggle(event: any, id: any) {

  }

  applyFilter(event: any) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  confirmDialog(id: any) {

  }

}
