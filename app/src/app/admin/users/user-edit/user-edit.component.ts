import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs';
import { Order } from 'src/app/models/order.model';
import { OrderService } from 'src/app/service/order.service';
import { environment } from 'src/enviroments/enviroment';
import { UserService } from '../../../service/user.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {

  data: any;
  paramId: any = 0;
  Errors = { status: false, msg: '' }
  myForm: FormGroup;
  submitted = false;
  url = "assets/img/product-1.jpg";

  categories : any =[];

  //--for edit data
  imagePath: string = environment.image_path;
  ImageDefault: any;

  id: string;
  mySubscription: any;


  constructor(
    private fb: FormBuilder,
    private router: Router,
    private userService: UserService,
    private actRoute: ActivatedRoute,
    public dialog: MatDialog
  ) {
  }


  addOrder() {
    // this.myForm.
  }
  addUsers() {
    this.userService
      .createUser(this.myForm.value)
      .subscribe((data: {}) => {
        console.log(data);
        this.router.navigate(['/admin/users']);
      });
  }
  ngOnInit(): void {
    this.id = this.actRoute.snapshot.params['id'];
    this.paramId = !this.id
    console.log(this.paramId);
    this.myForm = this.fb.group({
      id: [''],
      username: ['', Validators.required],
      fullName: ['', [Validators.required]],
      email: ['', Validators.email],
      address: ['', Validators.required],
      phone: ['',[Validators.required, Validators.pattern("[0-9 ]{10}")]],
      password: '123456',
      role: 1
    });
    if (!this.paramId) {
      this.userService.getOne(this.id)
        .subscribe(x => {
          console.log('get-one',x);
          return this.myForm.patchValue(x)
        }

        );
    }
  }
  get f() { return this.myForm.controls; }


  submit() {
    this.addUsers();
  }

  update() {
    this.userService.updateUser(this.id, this.myForm.value)
      .subscribe(data => {
        this.router.navigate(['/admin/users']);
      });
  }


  changeImageDefault(img: any) {

  }
  Urls: string[] = [];
  selectMultiple(e: any) {
    if (e.target.files) {
      for (let i = 0; i < File.length; i++) {
        var reader = new FileReader();
        reader.readAsDataURL(e.target.files[i]);
        reader.onload = (events: any) => {
          this.Urls.push(events.target.result)
        }
      }

    }
  }
}
