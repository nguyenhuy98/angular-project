import { Component } from '@angular/core';
import {  OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryStatus } from 'src/app/models/enum/category.contrains';
import { Voucher } from 'src/app/models/voucher.model';
import { VoucherService } from 'src/app/service/voucher.service';
import { environment } from 'src/enviroments/enviroment';

@Component({
  selector: 'app-vouchers',
  templateUrl: './vouchers.component.html',
  styleUrls: ['./vouchers.component.scss']
})
export class VouchersComponent implements OnInit{
  displayedColumns: string[] = [
    'id',
    'name',
    'description',
    'rate',
    'exemptions',
    'code',
  ];


  imgPath: string = environment.image_path;
  dataSource: MatTableDataSource<Voucher> = new MatTableDataSource();
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  Errors = { status: false, msg: '' };

  // categories = [
  //   "Quần", "Áo Khoác"
  // ];

  category: string;
  constructor(private httpApi: VoucherService) { }
  ngOnInit(): void {
    this.httpApi.getAll().subscribe((data) => {
      console.log(typeof data);

      this.dataSource.data = data;
    });
    console.log(this.dataSource.data);
    
  }

  applyFilter(event: any) { }
  onToggle(event: any, id: any) { }

  confirmDialog(id: any) { }
}
