import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryStatus } from 'src/app/models/enum/category.contrains';
import { ProductService } from 'src/app/service/product.service';
import { VoucherService } from 'src/app/service/voucher.service';
import { environment } from 'src/enviroments/enviroment';

@Component({
  selector: 'app-voucher-edit',
  templateUrl: './voucher-edit.component.html',
  styleUrls: ['./voucher-edit.component.scss']
})
export class VoucherEditComponent {
  data: any;
  paramId: any = 0;
  Errors = { status: false, msg: '' }
  myForm!: FormGroup;

  // images = []; //--for render show
  // multipleImages = []; //--for send to server

  categories = CategoryStatus;

  //--for edit data
  imagePath: string = environment.image_path;
  ImageDefault: any;


  mySubscription: any;

  constructor(
    private f: FormBuilder,
    private route: ActivatedRoute,
    private voucherService: VoucherService,
    private router: Router
  ) { }
  ngOnInit(): void {
    this.myForm = this.f.group({
      id: null,
      name: null,
      rate: null,
      code:null,
      exemptions: null,
      description: null,
    });

    this.paramId = this.route.snapshot.params['id'];
    if (this.paramId != undefined) {
      this.voucherService.getVoucherById(this.paramId).subscribe((data) => {
        this.myForm.setValue({
          id: data.id,
          name: data.name,
          code: data.code,
          rate: data.rate,
          exemptions: data.exemptions,
          description: data.description,
        });
      });
    }
  }
  submit() {
    const formValue = this.myForm.value;

    console.log(formValue);

    this.voucherService.save(formValue).subscribe(res => {
      this.router.navigate(["/admin/vouchers"])
    })
  }
  update(id: any) {
    this.myForm.value.id = id ;
    const formValue = this.myForm.value;
    // let formData: FormData = new FormData();
    // formData.append('id', id);
    // formData.append('name', formValue.name);
    // formData.append('code', formValue.code);
    // formData.append('rate', formValue.rate);
    // formData.append('description', formValue.description);
    // formData.append('exemptions', formValue.exemptions);


    this.voucherService.update(formValue).subscribe(res => {
      this.router.navigate(["/admin/vouchers"])
    })
  }

  changeImageDefault(img: any) { }

}
