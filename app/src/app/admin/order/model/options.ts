import { PaymentMethodValue } from "./enums";


export const PaymentMethodOption = [
  {label: 'Paypal', value: PaymentMethodValue.Paypal},
  {label: 'Payoneer', value: PaymentMethodValue.Payoneer},
  {label: 'CheckPayment', value: PaymentMethodValue.CheckPayment},
  {label: 'DirectBankTransfer', value: PaymentMethodValue.DirectBankTransfer},
  {label: 'CashonDelivery', value: PaymentMethodValue.CashonDelivery},

]
