import { Category } from 'src/app/models/category.model';
import { ProductService } from './../../../service/product.service';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Product } from 'src/app/models/product.model';
import { environment } from 'src/enviroments/enviroment';
import { User } from '../../../models/user.model';
import { Order } from 'src/app/models/order.model';
import { HttpClient } from '@angular/common/http';
import { OrderService } from '../../../service/order.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
  userOrder: any;
  imgPath: string = environment.image_path;
  displayedColumns: string[] = ['id','firstName','phone','createdTime','actions' ];
  dataSource = new MatTableDataSource<Order>()
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  Errors = {status:false, msg:''}

  categories :any = [];
  myForm : any
  constructor(
    private orderService: OrderService,
    public router: Router,
    private changeDetector: ChangeDetectorRef,
    private productService: ProductService
  ){

  }
  ngOnInit(): void {
    const objs:any = this.getAll();
    if(objs){
      console.log(objs);

      this.dataSource = new MatTableDataSource(objs);

      this.dataSource.paginator = this.paginator;
      console.log(this.dataSource.paginator);

      this.dataSource.sort = this.sort;
      this.changeDetector.detectChanges();
    }
    this.orderService.getAll().subscribe((data) => {
      //console.log(typeof data);
      this.dataSource.data = data;
      console.log(this.dataSource.data);
    });



  }



  getAll(){
    return this.orderService.getAll()
    .subscribe(data =>{
      this.dataSource.data = data;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }


  applyFilter(event:any){
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  onToggle(event:any,id:any){

  }

  confirmDialog(id:any){



  }

  delete(id:any){
    console.log('delete');
    console.log('id--', id);
    return this.orderService.deleteOrder(id).subscribe(data => {
      console.log(data);
      this.getAll()
    })
  }
}
