import { ProductService } from './../../../service/product.service';
import { Product } from './../../../models/product.model';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { first, filter } from 'rxjs';
import { OrderService } from 'src/app/service/order.service';
import { environment } from 'src/enviroments/enviroment';
import { PaymentMethodOption } from '../model/options';
import { CartAdminService } from 'src/app/service/cartAdmin.service';

@Component({
  selector: 'app-order-edit',
  templateUrl: './order-edit.component.html',
  styleUrls: ['./order-edit.component.scss']
})
export class OrderEditComponent implements OnInit {

  data: any;
  paramId: any = 0;
  check: any = 0;
  Errors = { status: false, msg: '' }
  myForm: FormGroup;
  submitted = false;
  url = "assets/img/product-1.jpg";

  dataCheck:any
  userOrder:any
  categories:any
  // orderProduct = [];
  //--for edit data
  imagePath: string = environment.image_path;
  ImageDefault: any;
  id: string;
  mySubscription: any;
  productOrders :any = []
  product: any;

  categoriess: any[] = PaymentMethodOption;


  constructor(
    private fb: FormBuilder,
    private router: Router,
    private orderService: OrderService,
    private actRoute: ActivatedRoute,
    public dialog: MatDialog,
    private productService: ProductService,
    private http: CartAdminService
  ) {
  }

  category:any;
  keyword:any
  getListProduct(){
    return this.productService.getAll(this.category, this.keyword).subscribe(
      data=>{
        this.userOrder = data;
        console.log(data);

      }
    )
  }
  addOrder() {
    this.orderService
      .createOrder({...this.myForm.value,productOrders:[]})
      .subscribe((data: {}) => {
        console.log(data);
        this.router.navigate(['/admin/orders']);
      });
    console.log(this.data);

  }

  update(){
    this.orderService.updateOrder(this.product)
      .subscribe(data => {
        this.data = this.productOrders.push(this.categories)
        console.log(data);
        this.router.navigate(['/admin/orders']);
      });
  }

  ngOnInit(): void {
    this.id = this.actRoute.snapshot.params['id'];
    this.paramId = !this.id
    console.log(this.paramId);
    this.myForm = this.fb.group({
      id:[''],
      firstName:[''],
      phone: [''],
      address: [''],
      lastName:[''],
      email:[''],
      paymentMethod:[''],
    });
    if (!this.paramId) {
      this.orderService.findOrder(this.id)
        .pipe(first())
        .subscribe(x => {
          console.log(x);
          this.product = x;

          const options = x?.orderProductRelationshipEntities?.map((item: any) => {
            return {
              label: item?.product?.name,
              price: item?.product?.price,
              quantity: item?.quantity,
              value: item.id,
              Total: item?.product?.price * item?.quantity,

            }
          })
          this.categories = options;
          return this.myForm.patchValue(x)
        }

        );
    }
  }
  get f() { return this.myForm.controls; }


  submit() {
    console.log(this.myForm.value);

    this.addOrder();
  }
  increamentQTY(id: any, quantity: Number){
    let orderProduct = this.product?.orderProductRelationshipEntities.filter((i:any) => i.id == id)[0];
    console.log(orderProduct);
    if(orderProduct) {
      orderProduct.quantity = orderProduct.quantity + quantity;
      if(orderProduct.quantity==0){
        this.product.orderProductRelationshipEntities = this.product?.orderProductRelationshipEntities.filter((i:any) => i.id !== id);
        console.log('test',this.product.orderProductRelationshipEntities)
        this.dataCheck =  this.product.orderProductRelationshipEntities
        if(this.dataCheck.length ===0){
          this.check = true
        }
        // if(this.product.orderProductRelationshipEntities == null){
        //   console.log(this.product.orderProductRelationshipEntities);
        //   this.check =true
        // }

       // this.product.orderProductRelationshipEntities = this.product?.orderProductRelationshipEntities.pop((i:any) => i.id !== id);
      }

    }

    const options = this.product?.orderProductRelationshipEntities?.map((item: any) => {
      return {
        label: item?.product?.name,
        price: item?.product?.price,
        quantity: item?.quantity,
        value: item.id,
        Total: item?.product?.price * item?.quantity,
      }
    })
    console.log('thay dổi',options);
    this.categories = options;
  }
  // abc(e:any){
  //   console.log(e);

  //   const selectProduct = this.product.orderProductRelationshipEntities.find((item: any) => item.id === e);

  //   this.myForm.patchValue({price: selectProduct.price})

  // }


  changeImageDefault(img: any) {

  }
  Urls: string[] = [];
  selectMultiple(e: any) {
    if (e.target.files) {
      for (let i = 0; i < File.length; i++) {
        var reader = new FileReader();
        reader.readAsDataURL(e.target.files[i]);
        reader.onload = (events: any) => {
          this.Urls.push(events.target.result)
        }
      }

    }
  }
}


