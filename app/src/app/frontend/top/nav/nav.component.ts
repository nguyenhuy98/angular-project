import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
  constructor(
    private router: Router,
  ) { }

  data:any

  menu = { main: false, child: false }
  mainOver() {
    this.menu.main = true;
  }
  mainOut() {
    this.menu.main = false;
  }
  childOver() {
    this.menu.main = false;
    this.menu.child = true;
  }
  childOut() {
    this.menu.child = false;
  }
  ngOnInit(): void {

  }

  logOut() {
    localStorage.removeItem("csrf");
    localStorage.removeItem("user_id");
    localStorage.removeItem("user_name");
    this.router.navigate([`home`]);
  }

  islogin() {
    if (localStorage.getItem("csrf")) {
      this.data = localStorage.getItem("user_name")
      console.log(typeof(this.data));
      return true;
    }
    return false;
  }



}
