import { Component, OnInit } from '@angular/core';
import { CartService } from "../../../service/cart.service";
import { of, Subscriber } from "rxjs";
import { CartAdminService } from "../../../service/cartAdmin.service";

@Component({
  selector: 'app-bottom-bar',
  templateUrl: './bottom-bar.component.html',
  styleUrls: ['./bottom-bar.component.scss']
})
export class BottomBarComponent implements OnInit {

  cart: any;
  constructor(private cartService: CartService,
    private adminCartService: CartAdminService) {
  }
  count: number;
  items$: Subscriber<any>;
  ngOnInit() {
    // xet timeout hien so luong sp  khi click them san pham vao gio hang
    this.count = JSON.parse(localStorage.getItem('cart') || '[]').length;
    this.adminCartService.storageSub$.asObservable().subscribe((res: any) => {
      setTimeout(() => {
        this.count = JSON.parse(localStorage.getItem('cart') || '[]').length

      }, 200)
    })
  }

  islogin() {
    if (localStorage.getItem("csrf")) {
      return true;
    }
    return false;
  }
}
