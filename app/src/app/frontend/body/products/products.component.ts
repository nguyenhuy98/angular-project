import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from 'src/app/service/product.service';
import { environment } from 'src/enviroments/enviroment';
import { CartAdminService } from "../../../service/cartAdmin.service";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  cart: any;
  products: any;
  imgPath: string = environment.image_path;
  category: any;
  constructor(
    private productService: ProductService,
    private router: Router,
    private activeRoute: ActivatedRoute
    ,
    private adminCartService: CartAdminService,

  ) {
  }
  ngOnInit(): void {
    this.activeRoute.params.subscribe((params: any) => {
      this.category = params
      this.search()
    });
  }

  getProductByCategory() {
    return this.productService.getProductByCategory(this.category["category"]).subscribe(
      data => {
        return this.products = data
      })
  }

  add2cart(qty: any, product: any) {
    let cartValue = JSON.parse(localStorage.getItem('cart') || '[]');
    const item = cartValue.find((c: any) => c.id === product.id)
    if (item) {
      cartValue = cartValue.map((c: any) => {
        if (c.id === product.id) {
          return { ...c, quantity: c.quantity + 1 };
        } else { return c }
      })

    } else {
      cartValue.push({ ...product, quantity: 1 })
    }
    this.adminCartService.storageSub$.next('');
    localStorage.setItem('cart', JSON.stringify(cartValue))
  }

  buynow() {
    this.router.navigate(["cart"]);
  }
  recentClick(slug: any) {

  }

  timeout: any;
  keyword: string;

  search() {
    return this.productService.getAll(this.category["category"], this.keyword).subscribe(
      data => {
        return this.products = data
      }
    )
  }

  triggerSearch() {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.search();
    }, 1000);
  }

}
