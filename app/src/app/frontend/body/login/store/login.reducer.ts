import { createReducer, on } from '@ngrx/store';
import * as loginActions from './login.actions';

export const loginKey = 'login';

export interface UserLoginState {
    loading: any;
    success: any;
    fail: any;
    userName: any;
}
const initLoginState: UserLoginState = {
    loading: false,
    success: false,
    fail: false,
    userName: ''
};


export const loginReducer = createReducer(
    initLoginState,
    on(loginActions.Login, (state, _) => ({
        ...state,
        loading: true
    })),
    on(loginActions.loginSuccess, (state, { success }) => ({
        ...state,
        success,
        loading: false
    })),
    on(loginActions.loginFail, (state, { error }) => {
        console.log('error', error);
        console.log('state', state);
        return ({
            ...state,
            fail: error,
            loading: false,

        });
    })
);