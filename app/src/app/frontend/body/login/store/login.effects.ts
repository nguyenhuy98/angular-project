import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { UserService } from 'src/app/service/user.service';
import * as loginActions from './login.actions';

@Injectable()
export class LoginEffects {

    constructor(
        private actions$: Actions,
        private userService: UserService,
        private router: Router
    ) { }

    loginService$ = createEffect(() => this.actions$.pipe(
        ofType(loginActions.Login),
        mergeMap(({ payload }) =>
            this.userService.login(payload).pipe(
                map((res) => {
                    return loginActions.loginSuccess({success: res})
                }),
                catchError((error) => of(loginActions.loginFail({ error: error.error.message })))
            )
        )
    )


    );
}
