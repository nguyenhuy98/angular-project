import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserService } from 'src/app/service/user.service';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { loginKey, loginReducer } from './store/login.reducer';
import { LoginEffects } from './store/login.effects';
import { LoginComponent } from './login.component';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';



@NgModule({
  declarations: [
    // LoginComponent
  ],
  imports: [
    CommonModule,
    StoreDevtoolsModule
  ],
})
export class LoginModule { }
