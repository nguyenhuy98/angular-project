import { Component, OnInit } from '@angular/core';
import { BannerService } from 'src/app/service/banner.service';
import { environment } from 'src/enviroments/enviroment';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {
  slideIndex = 0;
  images: any = [];
  imgPath: string = environment.image_path;
  page = 0;
  size = 1;
  totalPages = 0;
  constructor(private http: BannerService) {
  }
  ngOnInit(): void {
    this.showSlides(this.page, this.size);
  }
  showSlides(page: any, size: any) {
    this.http.getBanners(this.page, this.size).subscribe(
      res => {
        this.images = res.content;
        this.totalPages = res.totalPages;
      }
    )
  }
  prev() {
    if (this.page > 0) {
      this.page = this.page - 1;
    }
    this.showSlides(this.page, this.size);
  }
  next() {
    if (this.page < this.totalPages - 1) {
      this.page = this.page + 1;
    }
    this.showSlides(this.page, this.size);
  }
}
