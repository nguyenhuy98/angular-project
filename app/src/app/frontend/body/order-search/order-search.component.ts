import { Component, ElementRef, ViewChild } from '@angular/core';
import { OrderService } from 'src/app/service/order.service';

@Component({
  selector: 'app-order-search',
  templateUrl: './order-search.component.html',
  styleUrls: ['./order-search.component.scss']
})
export class OrderSearchComponent {
  @ViewChild('orderId', { read: ElementRef, static: false })
  orderId: ElementRef;

  order: any;
  error: number = 2;
  constructor(private orderService: OrderService) { }
  searchOrder() {
    this.orderService.findOrder(this.orderId.nativeElement.value).subscribe((data) => {
      this.order = data
      if (this.order !== null) {
        this.error = 0;
      } else {
        this.error = 1;
      }
    });
  }

}
