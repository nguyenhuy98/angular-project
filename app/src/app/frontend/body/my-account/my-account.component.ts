import { Component, ElementRef, Renderer2, ViewChild } from '@angular/core';
import { OrderService } from 'src/app/service/order.service';

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.scss'],
})
export class MyAccountComponent {
  @ViewChild('ordersTab', { read: ElementRef, static: false })
  orderList: ElementRef;
  @ViewChild('dashboard', { read: ElementRef, static: false })
  dashboard: ElementRef;
  @ViewChild('dashboardTab', { read: ElementRef, static: false })
  dashboardTab: ElementRef;
  @ViewChild('orders', { read: ElementRef, static: false }) orders: ElementRef;
  userOrder: any;
  list = [
    {
      name: 'Orders',
      id: 2,
      class: 'fa fa-shopping-bag'
    },
    {
      name: 'Address',
      id: 4,
      class: 'fa fa-map-marker-alt'
    },
    {
      name: 'Account Details',
      id: 5,
      class: 'fa fa-user'
    },
  ];
  selectedIdTab = 1;
  constructor(private order: OrderService) { }
  show(id: number) {
    this.selectedIdTab = id;
    if (id === 2) {
      let userId = window.localStorage.getItem('user_id');
      this.order.getOrderByUser(userId).subscribe((data: Array<any>) => {
        this.userOrder = [];
        data.forEach((item) => {
          this.userOrder.push(item);
        });
      });
    }


  }
}
