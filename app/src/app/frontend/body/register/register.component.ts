import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  NgForm,
  UntypedFormGroup,
  Validators
} from "@angular/forms";
import { SignupService } from 'src/app/service/signup.service';
import { Observable, Subject } from "rxjs";
import { Login } from "../login/store/login.actions";
import { HttpErrorResponse } from "@angular/common/http";
import { Router } from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  signupForm!: FormGroup;
  message = '';

  loading$!: Observable<any>;
  success$!: Observable<any>;
  error$!: Observable<any>;
  destroy$: Subject<void> = new Subject();

  constructor(private fb: FormBuilder, private http: SignupService, private rowter: Router) {
  }

  ngOnInit() {
    this.signupForm = this.fb.group({
      fullName: [null, Validators.required],
      email: [null, Validators.required],
      phone: [null],
      address: [null],
      username: [null, Validators.required, Validators.maxLength(2)],
      password: [null, [Validators.required, Validators.minLength(6), Validators.maxLength(15)]],
      confirmPassword: [null, [Validators.required]]
    },
      { validators: this.passwordMatchValidator });
  }


  get Username(): FormControl {
    return this.signupForm.get('username') as FormControl;
  }


  get Password(): FormControl {
    return this.signupForm.get('password') as FormControl;
  }

  get email(): FormControl {
    return this.signupForm.get('email') as FormControl;
  }

  get fullName(): FormControl {
    return this.signupForm.get('fullName') as FormControl;
  }
  get phone(): FormControl {
    return this.signupForm.get('phone') as FormControl;
  }
  get address(): FormControl {
    return this.signupForm.get('address') as FormControl;
  }

  get confirmPassword(): FormControl {
    return this.signupForm.get('confirmPassword') as FormControl;
  }

  signup() {
    console.log(this.signupForm)
    this.signupForm.markAllAsTouched();
    if (this.signupForm.invalid) {
      return;
    }
    this.http.signup(this.signupForm.value).subscribe((data) => {
      if (data.status == 'OK') {
        alert("Successful account registration! please log in")
        this.rowter.navigate([""])
      } else {
        alert(data.code)
      }
    }, (data: HttpErrorResponse) => {
      console.log(data)
      alert(data.error.code)
    })
  }

  passwordMatchValidator(control: AbstractControl): { [key: string]: boolean } | null {
    const password = control.get('password');
    const confirmPassword = control.get('confirmPassword');
    if (password && confirmPassword && password.value !== confirmPassword.value) {
      return { passwordMismatch: true };
    }
    return null;
  }

}
