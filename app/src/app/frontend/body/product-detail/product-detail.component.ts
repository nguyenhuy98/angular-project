import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from 'src/app/service/product.service';
import { environment } from 'src/enviroments/enviroment';
import { CommentService } from '../../../service/comment.service';
import { FormControl, FormGroup } from '@angular/forms';
import { UserService } from 'src/app/service/user.service';
import { Product } from 'src/app/models/product.model';
import { CartAdminService } from "../../../service/cartAdmin.service";

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss'],
})
export class ProductDetailComponent implements OnInit,OnChanges {
  // content:any;
  cart: any;
  product!: any;
  qtyDefault = 1;
  pImageDefault: any;
  imgPath: string = environment.image_path;
  id: any;
  showError = false;
  reviewSaved = false;
  //otherComments: Comment[];
  otherComments: any;
  reviewControl = new FormControl('');
  checkLogin: any;


  constructor(
    private actRoute: ActivatedRoute,
    private productService: ProductService,
    private adminCartService: CartAdminService,
    private commentService: CommentService,
    private userService: UserService,
    private router: Router
  ) { }
  ngOnInit(): void {
    this.checkLogin = localStorage.getItem('user_id');
    console.log(this.checkLogin);
    
    
    this.actRoute.params.subscribe((params: any) => {
      this.id = params.id;
      this.productService.getOne(this.id).subscribe((res: any) => {
        this.product = res;
        this.fetchAllReviews();
      });
    });
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.checkLogin = localStorage.getItem('user_id');

    if ( localStorage.getItem('user_id') ){

    }
    
  }
  fetchAllReviews() {
    this.otherComments = [];
    this.commentService
      .getAllReviewsOfProduct(this.product.id)
      .subscribe((data: any) => {
        for (let review of data) {
          this.otherComments.push(review);
        }
      });
  }

  minus() {
    console.log('minus');
    if (this.qtyDefault > 1) {
      this.qtyDefault--;
    }
  }
  plus() {
    console.log('plus');
    this.qtyDefault++;
  }

  add2cart(qtyDefault: any, product: any) {
    let cartValue = JSON.parse(localStorage.getItem('cart') || '[]');
    const item = cartValue.find((c: any) => c.id === product.id)
    if (item) {
      cartValue = cartValue.map((c: any) => {
        if (c.id === product.id) {
          return { ...c, quantity: c.quantity + 1 };
        } else { return c }
      })

    } else {
      cartValue.push({ ...product, quantity: 1 })
    }
    this.adminCartService.storageSub$.next('');
    localStorage.setItem('cart', JSON.stringify(cartValue))
  }

  buynow() {
    this.router.navigate([`cart`])
   }


  submitReview(id: any) {
    console.log(this.reviewControl.value ? this.reviewControl.value : "");
    console.log(this.router.url);
    const k = this.router.url.slice(16);
    console.log(k);

    const comment = {
      "content": this.reviewControl.value,
      "user": {
        id: localStorage.getItem('user_id')
      },
      "username":"Cong",
      "product": {
        id: k
      },
    }

    this.commentService.createComment(comment).subscribe(res => {
    })

    this.actRoute.params.subscribe((params: any) => {
      this.id = params.id;
      this.productService.getOne(this.id).subscribe((res: any) => {
        this.product = res;
        this.fetchAllReviews();
      });
      this.fetchAllReviews();
    });
    // this.fetchAllReviews();
    this.reviewControl.reset(); 
    // console.log(this.reviewControl.value);
    

  }

}
