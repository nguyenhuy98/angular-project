import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { OrderRequest } from 'src/app/models/orderRequest.model';
import { OrderService } from 'src/app/service/order.service';

@Component({
  selector: 'app-check-out',
  templateUrl: './check-out.component.html',
  styleUrls: ['./check-out.component.scss']
})
export class CheckOutComponent {

  constructor(
    private http: OrderService,
    private router: Router,
  ) { }

  orderRequest: OrderRequest;
  firstNameMsg = "";
  lastNameMsg = "";
  emailMsg = "";
  phoneMsg = "";
  addressMsg = "";
  paymentMethodMsg = "";
  message = "";
  total = 0;
  success = false;
  cart: Array<any>;
  ngOnInit(): void {
    this.initForm();
  }
  initForm() {
    let cartValue = JSON.parse(localStorage.getItem('cart') || '[]');
    this.cart = JSON.parse(localStorage.getItem('cart') || '[]');
    console.log('---------------', cartValue);
    let orderProduct = [];
    for (const item of cartValue) {
      this.total = this.total + Number(item.quantity) * Number(item.price);
      orderProduct.push({ productId: item.id, quantity: item.quantity });
    }
    this.orderRequest = {
      userId: Number(localStorage.getItem('user_id')) || 0,
      firstName: null,
      lastName: null,
      email: null,
      phone: null,
      address: null,
      paymentMethod: null,
      productOrders: orderProduct
    };
  }

  order:any;

  placeOrder(): void {
    this.http.placeOrder(this.orderRequest).subscribe(
      res => {
        localStorage.removeItem('cart');
        this.success = true;
        this.http.findOrder(res.id).subscribe(
          res => {
            this.order = res;
          });
      },
      err => {

        this.firstNameMsg = err.error.firstName ? 'First name ' + err.error.firstName : "";
        this.lastNameMsg = err.error.lastName ? 'Last name ' + err.error.lastName : "";
        this.emailMsg = err.error.email ? 'Email ' + err.error.email : "";
        this.phoneMsg = err.error.phone ? 'Phone ' + err.error.phone : "";
        this.addressMsg = err.error.address ? 'Address ' + err.error.address : "";
        this.paymentMethodMsg = err.error.paymentMethod ? 'Please choose a payment method!' : "";
        this.message = err.error.message;
        console.log('error ------', this.firstNameMsg);
      }
    )
  }

}
