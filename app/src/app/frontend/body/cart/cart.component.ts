import {Component, OnInit} from '@angular/core';
import { CartAdminService } from 'src/app/service/cartAdmin.service';
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  total = 0;
  ngOnInit(): void {
    this.getCart();
  }
  constructor(private http: CartAdminService) { }



  carts: any = {
    items: [],
    subTotal: 0,
    totalQuantity:0
  };
  getCart(): void {
    this.http.getCartItems().subscribe((data: any) => {
      this.carts = data;
    });

  }
  increamentQTY(item: any) {

    this.http.increaseQty(item).subscribe(() => {
      this.getCart();
    });
  }
  emptyCart(): void {
    this.http.emptyCart().subscribe(() => {
      this.getCart();
    });
  }
  deleteCart(item: any){
    this.http.deleteCart(item);
  }
  decreaseCart(item:any){
    this.http.decreaseCart(item).subscribe(() => {
        this.getCart();
      });

  }
}
