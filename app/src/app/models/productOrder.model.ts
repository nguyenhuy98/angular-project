export interface ProductOrderRequest {
    productId: number;
    quantity: number;
}