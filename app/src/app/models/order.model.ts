import { first } from 'rxjs';
export interface Order {
  id?: string;
  firstName: string;
  phone: string;
  createdTime?:Date;
}
