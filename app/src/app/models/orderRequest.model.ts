import { ProductOrderRequest } from "./productOrder.model";

export interface OrderRequest {
    userId?: number;
    firstName: any;
    lastName: any;
    email: any;
    phone: any;
    address: any;
    paymentMethod: any;
    productOrders: Array<ProductOrderRequest>
}