import { Product } from "./product.model";
import { User } from "./user.model";

export interface Comment{
  id?: number;
  product_id:number;
  content:string;
  user_id: string;
  createdAt?: string;
  user?: User;
  product?: Product;
}
