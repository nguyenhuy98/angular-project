export interface Voucher {
  id: string;
  name: string;
  description: string;
  rate: number;
  exemptions: number;
  code: string;
}
