import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of, Subject} from 'rxjs';
import {Product} from "../models/product.model";


@Injectable({
  providedIn: 'root'
})
export class CartAdminService {
  storageSub$= new Subject();
  constructor(private http: HttpClient) { }

  products: Array<Product> =[];

  carts: any =
    {
      items: [],
      subTotal: 0,
      totalQuantity: 0
    }

  getAllProducts() {
    return of(this.products);
  }

  addToCart(payload: any) {
    let isFind = false;
    for (let item of this.carts.items) {
      if (item._id == payload.productId) {
        item.quantity += payload.quantity;
        item.total = item.quantity * item.price;
        this.carts.totalQuantity += payload.quantity;
        isFind = true;
      }
    }

    if (!isFind) {
      const product = this.products.find(i => i.id == payload.productId);
      const totalProduct = product?.price;
      const item = {
        ...product,
        quantity: payload.quantity,
        total: totalProduct != undefined ? totalProduct * payload.quantity : undefined,
      }
      this.carts.items.push(item);
      this.carts.totalQuantity += payload.quantity;
    }
    return of({data: payload});
  }

  getCartItems() {
    const items = JSON.parse(localStorage.getItem('cart') || '[]');
    console.log(items);
    this.carts={items:items,subTotal:this.getSubtotal(items),totalQuantity:this.getTotalQuantity(items)}
    return of(this.carts);
  }

  increaseQty(payload: any) {
    const items=this.carts.items.map((e:any)=>{
      if(e.id==payload.id){
        return {...e,quantity:e.quantity+1}
      }else{ return e}
    })
    this.carts={items,subTotal:this.getSubtotal(items),totalQuantity:this.getTotalQuantity(items)};
    this.saveCartsToLocalStorage(this.carts);
    this.storageSub$.next('');
    return of(this.carts)
  }

  emptyCart() {
    localStorage.setItem('cart', '[]')
    this.carts.items = [];
    this.carts.totalQuantity = 0;
    this.carts.subTotal = 0;
    return of({message: "Thành công"});
  }
  decreaseCart(payload:any){
    let items=this.carts.items.map((e:any)=>{
      if(e.id==payload.id ){
        return {...e,quantity:e.quantity-1}
      }else{ return e}
    })
    items=items.filter((item:any)=> item.quantity >0)
    this.carts={items,subTotal:this.getSubtotal(items),totalQuantity:this.getTotalQuantity(items)};
    console.log(this.carts)
    this.saveCartsToLocalStorage(this.carts);
    this.storageSub$.next('');
    return of(this.carts)


  }
  saveCartsToLocalStorage(carts:any){
    localStorage.setItem('cart', JSON.stringify(carts.items))


  }

  getSubtotal(carts:any){
    if(carts){
      let total=0;
      carts.forEach((e:any)=>{
        total+=e.price*e.quantity
      })
      return total;
    }
    else return 0;


  }
  getTotalQuantity(carts:any){
    if(carts){
      let total =0;
      carts.forEach((e:any)=>{
        total+=e.quantity
      })
      return total;
    }else return 0

  }

  deleteCart(payload: any) {
    const items=this.carts.items.map((e:any)=>{
      if(e.id==payload.id){
        return {...e,quantity:e.quantity-1}
      }else{ return e}
    })
    this.carts={items,subTotal:this.getSubtotal(items),totalQuantity:this.getTotalQuantity(items)}
  }

  getQuantity(id: any) {
    let quantity = 1;
    return of({data: quantity});
  }

  getProduct(id: any) {
    debugger;
    const product = this.products.find(i => i.id === id);
    return of({data: product});
  }
}
