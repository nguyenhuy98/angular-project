import { HttpParams } from '@angular/common/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, retry, throwError } from 'rxjs';
import { environment } from 'src/enviroments/enviroment';
import { Product } from '../models/product.model';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  private readonly api: string;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private http: HttpClient) {
    {
      this.api = `${environment.api_URL}`;
    }
  }

  getAll(type: string, keyword: string): Observable<Product[]> {
    let params = new HttpParams();
    if (type) {
      params = params.set('type', type);
    }
    if (keyword) {
      params = params.set('keyword', keyword);
    }
    return this.http
      .get<Product[]>(this.api + '/product', { params })
      .pipe(retry(1), catchError(this.handleError));
  }

  getProductById(id: any): Observable<any> {
    return this.http.get(this.api + '/product/' + `${id}`);
  }

  getProductByCategory(category: string): Observable<any> {
    return this.http.get<Product>(`${this.api}/product/category/${category}`);
  }

  getOne(id: any) {
    return this.http.get<Product>(`${this.api}/product/${id}`)
      .pipe(retry(1), catchError(this.handleError));
  }

  update(body: any): Observable<any> {
    return this.http.put(this.api + '/product/', body, this.httpOptions);
  }

  save(body: any): Observable<any> {
    return this.http.post(this.api + '/product/', body, this.httpOptions);
  }

  deleteProduct(id: any): Observable<any> {
    return this.http.delete(this.api + `${id}`);
  }

  handleError(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
