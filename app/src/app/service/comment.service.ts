import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';
import { Observable, catchError, retry, throwError } from 'rxjs';
import { environment } from 'src/enviroments/enviroment';

@Injectable({
  providedIn: 'root'
})
export class CommentService  {
  private readonly api:string
  constructor(private http: HttpClient) {
    {
      this.api = `${environment.api_URL}`
     }
   }

   httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  getAllComment(): Observable<Comment>{
    return this.http.get<Comment>(`${this.api}/comment/list`).pipe(retry(1), catchError(this.handleError));
  }


  getOne(id:any): Observable<Comment>{
    return this.http.get<Comment>(`${this.api}/comment/${id}`)
    .pipe(retry(1), catchError(this.handleError));
  }
  submitReview(userid: number, productid: number, review: string) {
    let obj: any = {
      User: {
        Id: userid,
      },
      Product: {
        Id: productid,
      },
      Value: review,
    };

    let url = this.api + '/comment';
    return this.http.post(url, obj, { responseType: 'text' });
  }


  createComment(comment: any): Observable<any> {
    return this.http
      .post<any[]>(
        this.api + '/comment/',
        JSON.stringify(comment),
        this.httpOptions
      )
      .pipe(retry(1), catchError(this.handleError));
  }

  getAllReviewsOfProduct(productId: any) {
    return this.http.get<Comment>(`${this.api}/comment/list/${productId}`).pipe(retry(1), catchError(this.handleError));
  }

  handleError(error:any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }




}
