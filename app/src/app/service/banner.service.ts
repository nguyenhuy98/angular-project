import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BannerService {

  constructor(private http: HttpClient) { }

  getBanners(page: any, size: any) {
    return this.http.get<any>(`http://localhost:8080/banner`, { params: { page: page, size: size } });
  }
}
