import { environment } from 'src/enviroments/enviroment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, retry, throwError } from 'rxjs';
import { Order } from '../models/order.model';
import { OrderRequest } from '../models/orderRequest.model';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private readonly api: string

  constructor(private http: HttpClient) {
    this.api = `${environment.api_URL}`
  }
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  getAll(): Observable<Order[]> {

    return this.http
      .get<Order[]>(this.api + '/order')
      .pipe(retry(1), catchError(this.handleError));
  }


  updateOrder(order: Order) : Observable<Order> {
    return this.http
    .put<Order>(
      `${this.api}/order`,
      JSON.stringify(order),
      this.httpOptions
    )
    .pipe(retry(1), catchError(this.handleError));
  }

  createOrder(order: Order): Observable<Order>{
    return this.http.post<Order>(
      this.api + '/order/',
      JSON.stringify(order),
      this.httpOptions
    )
    .pipe(retry(1), catchError(this.handleError));
  }

  placeOrder(order: OrderRequest) {
    return this.http.post<any>(this.api + `/order`, order, this.httpOptions)
  }


  getOne(id: any): Observable<Order> {
    return this.http.get<Order>(`${this.api}/order/${id}`).pipe(retry(1), catchError(this.handleError));
  }

 //my-account
  getOrderByUser(id: any): Observable<any> {
    return this.http.get(`${this.api}/order/user`, {
      'params': {
        'userId': id
      }
    });
  }

  findOrder(id: any): Observable<any> {
    return this.http.get(`${this.api}/order/detail/${id}`);
  }

  deleteOrder(id:any):  Observable<any>{
    return this.http
      .delete<Order>(`${this.api}/order/${id}`, this.httpOptions)
      .pipe(retry(1), catchError(this.handleError))
  }

  handleError(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }


}
