import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, retry, catchError, throwError } from 'rxjs';
import { User } from '../models/user.model';
import { environment } from 'src/enviroments/enviroment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private readonly api: string
  constructor(private http: HttpClient) {
    {
      this.api = `${environment.api_URL}`
    }
  }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  login(payload: Object) {
    return this.http.post<any>(`http://localhost:8080/login`, payload, this.httpOptions);
  }



  getUsers(): Observable<User[]> {
    return this.http
      .get<User[]>('http://localhost:8080/user')
      .pipe(retry(1), catchError(this.handleError));
  }

  createUser(user: User): Observable<User> {
    return this.http
      .post<User>(
        this.api + '/user',
        // this.api + '/user/signup',
        JSON.stringify(user),
        this.httpOptions
      )
      .pipe(retry(1), catchError(this.handleError));
  }


  updateUser(id: string, user: User): Observable<User> {
    return this.http
      .put<User>(
        `${this.api}/user/${id}`,
        JSON.stringify(user),
        this.httpOptions
      )
      .pipe(retry(1), catchError(this.handleError));
  }

  getOne(id: any) {
    return this.http.get<User>(`http://localhost:8080/user/${id}`)
      .pipe(retry(1), catchError(this.handleError));
  }

  deleteUser(id: any) {
    return this.http
      .delete<User>(`${this.api}/user/${id}`, this.httpOptions)
      .pipe(retry(1), catchError(this.handleError));
  }


  handleError(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
