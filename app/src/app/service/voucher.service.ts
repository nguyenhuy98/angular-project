import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, retry, throwError } from 'rxjs';
import { environment } from 'src/enviroments/enviroment';
import { Product } from '../models/product.model';
import { Voucher } from '../models/voucher.model';

@Injectable({
  providedIn: 'root',
})

export class VoucherService {
  private readonly api: string;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private http: HttpClient) {
    {
      this.api = `${environment.api_URL}`;
    }
  }

  getAll(): Observable<Voucher[]> {
    // let param = { params: { } }
    // if(type) {
    //   param.params = {type : type};
    // }
    // return this.http
    //   .get<Voucher[]>(this.api + '/voucher', param)
    //   .pipe(retry(1), catchError(this.handleError));
    return this.http.get<Voucher[]>(this.api + '/voucher/list');
  }

  getVoucherById(id: any): Observable<any> {
    return this.http.get(this.api + `${id}`);
  }

  // getVoucherByCategory(category: string): Observable<any> {
  //   return this.http.get<Product>(`${this.api}/voucher/category/${category}`);
  // }

  // getOne(id:any){
  //   return this.http.get<Product>(`${this.api}/product/${id}`)
  //   .pipe(retry(1), catchError(this.handleError));
  // }

  update(body: any): Observable<any> {
    return this.http.put(this.api+'/voucher/', body, this.httpOptions);
  }

  save(body: any): Observable<any> {
    return this.http.post(this.api+'/voucher/', body, this.httpOptions);
  }

  deleteVoucher(id: any): Observable<any> {
    return this.http.delete(this.api + `${id}`);
  }

  handleError(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
