import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class SignupService {

  constructor(private http: HttpClient) { }
  signup(payload: Object) {
    return this.http.post<any>(`http://localhost:8080/user/signup`, payload);
  }
}
