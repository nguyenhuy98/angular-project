import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { en_US } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { IconsProviderModule } from './icons-provider.module';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { AdminRoutingModule } from './admin/admin.routing.module';
import { FrontEndRoutingModule } from './frontend/frontend.routing.module';
import { MaterialModule } from './shared/material-module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { VouchersComponent } from './admin/voucher/vouchers/vouchers.component';
import { VoucherEditComponent } from './admin/voucher/voucher-edit/voucher-edit.component';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    // VouchersComponent,
    // VoucherEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FrontEndRoutingModule,
    AdminRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    HttpClientModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: false,
      autoPause: true
    }),
  ],
  providers: [
    { provide: NZ_I18N, useValue: en_US }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
